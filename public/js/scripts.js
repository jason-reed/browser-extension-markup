(function($){

$(".m-input__clear").click(function() {
	$(this).parents().find('input').val('').focus();
});

$('.m-dropdown__item').click(function() {
	$('#application').attr('value', $(this).attr('data-value'));
});

$('.m-item__action').click(function(){
	$(this).parents('.m-list__item').remove();
});

})(jQuery);